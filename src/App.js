/* eslint-disable */
import "./App.css";
import React, { useState } from "react";

function App() {
  const list = [
    "Banana",
    "Apple",
    "Orange",
    "Mango",
    "Pineapple",
    "Watermelon",
  ];

  const [filteredList, setFilteredList] = useState(list);

  const changeHandler = (event) => {
    console.log(event.target.value);

    if (event.target.value === "") {
      setFilteredList(list);
    }

    // const filteredDetails = list.filter(elm => elm.toLowerCase() == event.target.value.toLowerCase());
    // console.log('filteredDetails', filteredDetails);
    // setFilteredList(filteredDetails);
    const filteredValues = list.filter(
      (item) =>
        item.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1
    );
    console.log(filteredValues);
    setFilteredList(filteredValues);
  };

  const listDetails = filteredList.map((e) => {
    return <div key={e}>{e}</div>;
  });
  return (
    <div className="App">
      search:
      <input type="text" name="query" onChange={changeHandler}></input>
      <div>{filteredList ? listDetails : ""}</div>
    </div>
  );
}

export default App;
